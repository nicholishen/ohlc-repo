//+------------------------------------------------------------------+
//|                                            CandleMarketOrder.mq4 |
//|                                      Copyright 2017, nicholishen |
//|                         https://www.forexfactory.com/nicholishen |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, nicholishen"
#property link "https://www.forexfactory.com/nicholishen"
#property version "1.00"
#property strict
#define MAGIC 6654654
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
datetime last_trade_time;

int OnInit()
{
   //---
   last_trade_time = TimeCurrent();

   //---
   return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
   //---
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   if (Time[0] > last_trade_time && OrdersForBar() <= 0)
   {
      if (Close[1] < Open[1])
      {
         if (OrderSend(_Symbol, OP_BUY, 0.01, Ask, 0, 0, 0) >= 0)
            last_trade_time = TimeCurrent();
      }
      else if (Close[1] > Open[1])
      {
         if (OrderSend(_Symbol, OP_SELL, 0.01, Bid, 0, 0, 0) >= 0)
            last_trade_time = TimeCurrent();
      }
   }
}
//+------------------------------------------------------------------+

int OrdersForBar()
{
   int cnt = 0;
   for (int i = OrdersTotal() - 1; i >= 0; i--)
      if (OrderSelect(i, SELECT_BY_POS) && OrderSymbol() == _Symbol && OrderMagicNumber() == MAGIC)
         if (OrderOpenTime() >= Time[0])
            cnt++;

   for (int i = OrdersHistoryTotal() - 1; i >= 0; i--)
      if (OrderSelect(i, SELECT_BY_POS, MODE_HISTORY) && OrderSymbol() == _Symbol && OrderMagicNumber() == MAGIC)
         if (OrderOpenTime() >= Time[0])
            cnt++;

   return cnt;
}